#


import sys

def BinaryRepresentation(number):
    #implement your code here
    ##
    ##  this function takes a
    #  positive integer number as parameter
    ##  returns string with the binary representation of the number
    ##  ejemplo: 
    ##              BinaryRepresentation(21) returns the value "10101"
    ##              BinaryRepresentation(5) returns the value "101"
    
    binary = "---------"
    return binary

def BinaryRepresentationEntry():
    if len(sys.argv) == 1:
        print ("Parameter expected")
        return
    
    if len(sys.argv) > 2:
        print("Too Many Arguments")
        return
    
    if len(sys.argv) == 2:
        num = int(sys.argv[1])
        print("The binary representation of {0} is {1}".format(num,BinaryRepresentation(num)))
        return


def main():
    BinaryRepresentationEntry()

if __name__ == "__main__":
    main()