﻿using System;
    class MainClass
    {
        public static void Main(string[] args)
        {
            BinaryRepresentationEntry(args);
        }

        private static void BinaryRepresentationEntry(string[] args)
        {
            if (args.Length == 0)
            {
                System.Console.WriteLine("Parameter expected");
                return;
            }
            if (args.Length > 1)
            {
                System.Console.WriteLine("Too Many Arguments");
                return;
            }
            if (args.Length == 1)
            {
                System.Int64 num = System.Int64.Parse(args[0]);
                string binary = BinaryRepresentation(num);
                System.Console.WriteLine("La representación Binaria de {0} es {1}", num, binary);
                return;
            }
        }

        private static string BinaryRepresentation(System.Int64 number)
        {
            // implement your code here
            //
            //   this function takes a positive integer number as parameter
            //   returns string with the binary representation of the number
            return "-------";
        }
    }
