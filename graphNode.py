
class Node:
    def __init__(self,info):
        self.info = info   #info puede ser cualquier objeto.
        self.relaciones = []  #un geafo puede apuntar a muchos otros grafos

    def getInfo(self):return self.info

    # Un nodo se relaciona adelante, y le pide al otro nodo que se relacione
    # Siempre se estabece una relación bilateral para grafos no dirigidos.
    def relacionarCon(self, nodo):
        if  str(type(nodo)) == "<class '__main__.Node'>": #si el nodo a enlazar es válido
            #verificar que ya no exista la relación, para no crear relaciones redundantes
            for n in self.relaciones:
                if n is nodo:
                    return     #ya existe la relación. No se hace necesario crear otra
            
            #se establece la relación
            self.relaciones.append(nodo)

            #se solicita al otro nodo establecer relación
            nodo.relacionarCon(self)

    #este método lista las relaciones del nodo
    def mostrarRelaciones(self):
        for n in self.relaciones:
            print(n.getInfo(),end='\n')

    #este método lista las relaciones del nodo
    def obtenerListaDeRelaciones(self):
        lista = []
        for n in self.relaciones:
            lista.append(n)
        return lista
    
def crearGrafoEstrellita():
    #ejemplo de un grafo estrella, donde hay un nodo central y 5 nodos relacionados al centro
    centroEstrella = Node(info=["nodo al centro", 3.141592, 2.718281])
    for i in range(1,6):
        nodoRandom = Node(["punta de estrella ", i])
        centroEstrella.relacionarCon(nodoRandom) #solicitar al nuevo grafo que se relacione
    return centroEstrella #devolver la referencia al nodo central


def main():
    grafo  = crearGrafoEstrellita()
    print('\n')
    print("+++++++++++++++++++++++++++++++++++++++",end='\n')
    print("se creó el grafo ", grafo.getInfo())
    print('lista de grafos relacionados...',end='\n')
    print("---------------------------------------",end='\n')
    
    #mostrar las relaciones del nodo central
    grafo.mostrarRelaciones()

    #mostrar las relaciones de
    listaDeRelaciones = grafo.obtenerListaDeRelaciones()
    
    #preguntarle a cada nodo relacionado, por sus relaciones.
    #si todo anda bien, todos los nodos sólo me mostrarán la relación al centro.
    print("---------------------------------------",end='\n')
    print('lista de info de grafos relacionados...',end='\n')
    print("---------------------------------------",end='\n')
    for n in listaDeRelaciones:
        infoNodo = n.getInfo()
        relsNodos = n.obtenerListaDeRelaciones()
        print("Soy el nodo ",infoNodo, "y me relaciono con: ", end='\n')
        for r in relsNodos:
            print("              -------->",r.getInfo(),end='\n')


if __name__ == "__main__":
    main()