import sys
def crearDic(listaClaves, listaValores):
    dic = {}
    for i in range(len(listaClaves)):
        dic[listaClaves[i]] = listaValores[i]
    return dic

def existe(dic, el):
    return el in dic

def getValoresDesdeLineaDeComandos():
    lv =[]
    lk =[]
    for i in range(1,len(sys.argv)-1,2):
        lk.append(sys.argv[i])
        lv.append(sys.argv[i+1])
    return lk, lv

def main():
    lk, lv = getValoresDesdeLineaDeComandos()
    midic = crearDic(lk, lv)
    print(existe(midic, "H"))
    print(existe(midic, "I"))







if __name__ == "__main__":
    main()