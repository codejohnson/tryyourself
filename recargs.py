import sys
# si una funcion se usa para llamarla, 
# nada evita que se pueda llamar ella misma

#una suma de N numeros, ejemplo no recursivo
def suma(n):
    s=0
    for i in range(n+1):
        s += i
    return s
#una suma de N numeros, ejemplo recursivo
def rsuma(n):
    if n == 0:
        return 0
    return n + rsuma(n-1)

#imprimir una liata, n recursivo
def printlist(thelist):
    for e in thelist:
        print(e, end=" ")
    print()

def rprintlist(thelist):
    if len(thelist) == 0:
        return
    print(thelist[0],end=";")
    rprintlist(thelist[1:])

def buscarlineal(thelist, elem):
    for e in thelist:
        if e== elem:
            return True
    return False

def rbuscarlineal(thelist, elem):
    if len(thelist) == 0:
        return False
    if thelist[0] == elem:
        return True
    else:
        return rbuscarlineal(thelist[1:],elem)

def cargarListaDesdeLineaComando():
    lista = []
    for i in range(1,len(sys.argv)):
        lista.append(sys.argv[i])
    return lista

def presentarArgumentos():
    for a in sys.argv:
        print(a)

def main():
    #presentarArgumentos()
    #n=int(sys.argv[1])
    #print(suma(n))
    #print(rsuma(n))
    al = cargarListaDesdeLineaComando()
    printlist(al)
    rprintlist(al)
    #print(buscarlineal(al,25))
    #print(buscarlineal(al,27))
    #print(rbuscarlineal(al,25))
    #print(rbuscarlineal(al,27))

if __name__ == "__main__":
    main()