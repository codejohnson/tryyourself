
import sys

def saltolinea(): print('\n')

def main():
    saltolinea()
    print("----------")
    print("me pasaste",len(sys.argv),'argumentos.', end='\n')
    print("'argv' es una lista formada por los argumentos entre los espacios",end='\n')
    print("a continuación se presenta la lista de argumentos que pasaste:",end='\n')
    saltolinea()
    print(".......................")
    for i in range(1,len(sys.argv)):
        print("parámetro",i, "es -->",sys.argv[i],end='\n')
    print(".......................")
    saltolinea()
    print("Esto es muy útil, porque cada parámetro se usa para distintas acciones.",end='\n')
    saltolinea()
#end main


    
if __name__ == "__main__":
    main()
